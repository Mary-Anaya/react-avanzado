import Logeando from '../Utils/Logeando';
import { LoginContext } from '../components/LoginContext';
import { useContext } from 'react';

export default function useSesion() {
  const contexto = useContext(LoginContext);

  const login = async (gmail, passw) => {
    try {
      const respuesta = await Logeando({
        email: gmail,
        password: passw,
      });
      contexto.cambioSesion({
        estaLogeado: true,
        token: respuesta.token,
      });
    } catch (error) {
      console.log(error.message);
    }
  };

  return { login };
}
