import axios from 'axios'

const URL = 'https://reqres.in/api'

// Tipos de acciones

export const LOGIN = 'LOGIN';
export const CERRAR_SESSION = 'CERRAR_SESSION';
export const GET_USUARIOS = 'GET_USUARIOS'

export function autenticar(datos) {
  return dispatch => {
     axios.post(`${URL}/login`, datos).then(respuesta => { 
      const token = respuesta.data.token;
      localStorage.setItem('__token__', token)
      dispatch({ type: LOGIN, token })
    })
  }
}

export function getUsuarios() {
  return dispatch => {
    axios.get(`${URL}/users`).then(respuesta => { 
     dispatch({type: GET_USUARIOS, usuarios: respuesta.data})
    })
  }
}