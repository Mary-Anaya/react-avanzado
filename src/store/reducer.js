import { CERRAR_SESSION, GET_USUARIOS, LOGIN } from './action';

const initialState = {
  logueado: false,
  token: null,
  listaUsuarios: null,
};

export function autenticacionReducer(estado = initialState, accion) {
  if (accion.type === LOGIN) {
    return { logueado: true, token: accion.token };
  }

  if (accion.type === CERRAR_SESSION) {
    return initialState;
  }

  if (accion.type === GET_USUARIOS) {
    return { ...estado, listaUsuarios: accion.usuarios };
  }

  return estado;
}
