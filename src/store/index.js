import { applyMiddleware, combineReducers, compose, createStore } from 'redux'

import {autenticacionReducer} from './reducer'
import thunk from 'redux-thunk'

/**
 * Reducers
 */

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const rootReducer = combineReducers({
  usuario: autenticacionReducer
})


/**
 * Crear el store
 */
export const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(thunk))
)