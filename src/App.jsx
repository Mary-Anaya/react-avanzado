import { LoginProvider, informacionSesion } from './components/LoginContext';
import { Navigate, Outlet, Route, Routes } from 'react-router-dom';

import { Contacto } from './pages/Contacto';
import { Header } from './components/Header';
import { Informacion } from './pages/Informacion';
import { Login } from './pages/Login';
import { Portada } from './pages/Portada';
import { Provider } from 'react-redux';
import ThemeProvider from './Utils/ThemeContext';
import { store } from './store';
import { useSelector } from 'react-redux';

function RutaPrivada({ children, ...props }) { 
  const { logueado } = useSelector(store => store.usuario)
  if (!logueado) {
    return <Navigate to="/login" />
  }
  return <Outlet/>
} 

function App() {
  return (
    <Provider store={store}>
      <LoginProvider inicialValueSesion={informacionSesion}>
        <ThemeProvider>
          <div>
            <Header />
            <Routes>
              <Route path='/' element={<RutaPrivada />} >
                 <Route index element={<Portada />} />
                 <Route path='/contacto' element={<Contacto />} />
                 <Route path='/informacion' element={<Informacion />} />
              </Route>
              <Route path='/login' element={<Login />} />
            </Routes>
          </div>
        </ThemeProvider>
      </LoginProvider>
    </Provider>
  );
}

export default App;