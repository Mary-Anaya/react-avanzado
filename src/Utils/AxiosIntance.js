import axios from 'axios';

const instancia = axios.create({ baseURL: 'https://reqres.in/api' });

export default instancia;
