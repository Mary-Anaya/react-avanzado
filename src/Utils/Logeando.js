import instancia from './AxiosIntance'

export default async function Logeando({email, password}){
    let respuesta = await instancia.post("/login", {email:email, password:password})

    return respuesta.data


}