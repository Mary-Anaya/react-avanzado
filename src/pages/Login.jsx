import { useDispatch, useSelector } from 'react-redux';

import LoginForm from '../components/LoginForm';
import {autenticar} from '../store/action'
import { useNavigate } from "react-router-dom";
import { useRef } from 'react';

export function Login() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const {logueado} = useSelector((store) => store.usuario);
  
  const ref = useRef(null);

  const printLogin = (values) => dispatch(autenticar(values))

  if (logueado) {
    navigate('/informacion')
  }

  return (
    <div className='container'>
      <div className='d-grid' style={{ placeItems: 'center' }}>
        <LoginForm onSubmit={printLogin} innerRef={ref} />
        <button
          className='btn btn-primary'
          onClick={() => ref.current.submitForm()}>
          Login
        </button>
      </div>
    </div>
  );
}