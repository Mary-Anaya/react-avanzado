import { ContactoForm } from '../components/ContactoForm';
import { useRef } from 'react';

export function Contacto() {
  const ref = useRef(null);

  const printLogin = (values) => {
    console.log(values);
  };

  return (
    <div className='container'>
      <div className='d-grid' style={{ placeItems: 'center' }}>
        <ContactoForm onSubmit={printLogin} innerRef={ref} />
        <button
          className='btn btn-primary'
          onClick={() => ref.current.submitForm()}>
          {' '}
          Enviar{' '}
        </button>
      </div>
    </div>
  );
}
