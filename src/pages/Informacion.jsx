import { useDispatch, useSelector } from 'react-redux';

import { Card } from '../components/Cards';
import { getUsuarios } from '../store/action';
import { useEffect } from 'react';

export function Informacion() {
  const { listaUsuarios } = useSelector((store) => store.usuario);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getUsuarios());
  }, []);

  return (
    <div>
      <div
        id='portada'
        className='d-flex flex-column justify-content-center align-items-center'>
        <h1>Información</h1>

        <div className='row py-3'>
          {listaUsuarios
            ? listaUsuarios.data.map((item) => <Card key={item.id} {...item} />)
            : null}
        </div>
      </div>
      <div className='container'></div>
    </div>
  );
}
