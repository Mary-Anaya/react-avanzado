
export function Portada() {
  return (
      <div>
        <div
          id='portada'
          className='d-flex flex-column justify-content-center align-items-center'>
          <h1>Portada</h1>
        </div>
        <div className='container'>
          <p className='mt-4'>
            loremVelit esse consectetur amet laborum sunt laborum qui non
            aliquip nulla ex ea deserunt qui. Nostrud est Lorem non in labore
            exercitation ipsum sint veniam. Nostrud laborum aute elit dolor id
            laborum do non excepteur. Culpa quis amet nostrud excepteur proident
            amet reprehenderit et ut. Esse occaecat est veniam adipisicing ea
            sit irure amet labore nisi ut ut esse aliqua. Dolor cillum officia
            non ea irure sint non et irure ullamco commodo dolore labore. Sint
            commodo laborum irure aliqua in nisi cillum laboris enim. Amet
            aliqua officia quis velit in culpa. Ea consequat enim esse duis duis
            consectetur adipisicing cupidatat Lorem. Anim irure reprehenderit
            dolore commodo fugiat duis. Cillum dolor esse enim incididunt. Do
            anim fugiat tempor cupidatat quis ex ad sit aliquip quis. Eu aliqua
            nisi Lorem magna in occaecat voluptate ut. Laborum cupidatat
            cupidatat excepteur excepteur ea ipsum. Consequat culpa do sunt
            fugiat occaecat dolor esse officia incididunt aliquip. Ipsum qui
            occaecat fugiat laborum cupidatat pariatur ea excepteur voluptate
            excepteur. Deserunt sint esse anim in. Sunt adipisicing nostrud sint
            nulla sint incididunt nisi cupidatat. Id ut mollit id irure.
          </p>
        </div>
      </div>
  );
}
