export function Card({ first_name, last_name, avatar, email }) {
  const fullName = `${first_name} ${last_name}`
  return (
    <div className='col-sm-2 col-xs-12'>
      <div className='card' style={{ width: '18rem' }}>
      <img src={avatar} className='card-img-top' alt={fullName} />
      <div className='card-body'>
        <h5 className='card-title'>
          {fullName}
        </h5>
        <p className='card-text'>{email}</p>
      </div>
    </div>
    </div>
  );
}
