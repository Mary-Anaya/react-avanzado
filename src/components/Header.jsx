import { Fragment, useContext } from 'react';

import { Link } from 'react-router-dom';
import { ThemeContext } from '../Utils/ThemeContext';

export function Header() {
  const { toggle_theme } = useContext(ThemeContext);

  return (
    <Fragment>
      <nav className='nav navbar-dark py-2 mb-5'>
        <div className=' mx-3 div'>
          <Link className='navbar-bran' to='/'>
            Portada
          </Link>
          <Link className='navbar-bran' to='/informacion'>
            Información
          </Link>
          <Link className='navbar-bran' to='/login'>
            Login
          </Link>
          <Link className='navbar-bran' to='/contacto'>
            Contacto
          </Link>
        </div>

        <button className='btn btn-light' onClick={() => toggle_theme()}>
          Cambiar tema
        </button>
      </nav>
    </Fragment>
  );
}
