import { Field, Form, Formik } from 'formik';

export default function LoginForm({ onSubmit, innerRef }) {
  const initialData = {
    email: '',
    password: '',
  };

  const submit = (values, actions) => {
    actions.setSubmitting(true);
    onSubmit(values);
    actions.resetForm();
    actions.setSubmitting(false);
  };

  return (
    <Formik initialValues={initialData} onSubmit={submit} innerRef={innerRef}>
      {() => {
        return (
          <div>
            <h1 className='text-center'>Login</h1>
            <Form>
              <div className='form-group my-3'>
                <label htmlFor='username'>Correo Electronico</label>
                <Field
                  className='form-control'
                  id='username'
                  type='email'
                  name='email'
                  placeholder='Ingrese su nombre'></Field>
              </div>
              <div className='form-group my-3'>
                <label htmlFor='pass'>Contraseña</label>
                <Field
                  className='form-control'
                  id='pass'
                  type='password'
                  name='password'
                  placeholder='Ingrese su contraseña'></Field>
              </div>
            </Form>
          </div>
        );
      }}
    </Formik>
  );
}
