import { createContext, useState } from 'react';

const informacionSesion = {
  estaLogeado: false,
  token: '',
};

const LoginContext = createContext(informacionSesion);

const { Provider, Consumer } = LoginContext;

const LoginProvider = ({ children, inicialValueSesion }) => {
  const [session, setSesion] = useState(inicialValueSesion);

  const cambioSesion = (nuevaSesion) => {
    setSesion(nuevaSesion);
  };
  return <Provider value={{ session, cambioSesion }}>{children}</Provider>;
};

export { LoginProvider, Consumer, LoginContext, informacionSesion };
