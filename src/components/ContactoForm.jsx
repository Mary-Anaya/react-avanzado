import { Field, Form, Formik } from 'formik';

export function ContactoForm({ onSubmit, innerRef }) {
  const initialData = {
    user_name: '',
  };

  const submit = (values, actions) => {
    actions.setSubmitting(true);
    onSubmit(values);
    actions.setSubmitting(false);
  };

  return (
    <Formik initialValues={initialData} onSubmit={submit} innerRef={innerRef}>
      {() => {
        return (
          <div>
            <h1 className='text-center'>Contacto</h1>
            <Form>
              <div className='form-group my-3'>
                <label htmlFor='username'>Nombre de usuario</label>
                <Field
                  className='form-control'
                  id='username'
                  type='text'
                  name='user_name'
                  placeholder='Ingrese su nombre'></Field>
              </div>
              <div className='form-group my-3'>
                <label htmlFor='text'>Mensaje</label>
                <Field
                  as='textarea'
                  rows='10'
                  cols='50'
                  className='form-control'
                  id='text'
                  name='textarea'
                  placeholder='Ingrese un mensaje'></Field>
              </div>
            </Form>
          </div>
        );
      }}
    </Formik>
  );
}
